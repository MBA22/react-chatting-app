var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var UserChat = require('UserChat');
var actions = require('./../actions/index');
var Main = React.createClass({

    componentDidMount: function () {
        this.props.getUsers();
    },
    logout: function () {
        this.props.logout();
    },
    chatWithUser: function (userKey, userName) {
        this.props.chatWith(userKey, userName);
    },
    render: function () {
        var totalUsers = this.props.users.users;
        if (this.props.users.userKey !== undefined && this.props.users.userKey != null) {
            return (
                <UserChat></UserChat>
            );
        }
        if (this.props.users.fetching) {
            return (
                <p>Loading...</p>
            );
        }
        else {
            if (totalUsers != null || totalUsers != undefined) {
                return (
                    <div className="container">
                        <h2 className="text-center">Users</h2>
                        {
                            Object.keys(totalUsers).map(function (key) {
                                if (key !== this.props.users.uid) {
                                    return (
                                        <div className="col-xs-4 col-xs-offset-4" key={key}>{totalUsers[key].name+"   "}
                                            <button className="btn btn-success"
                                                    onClick={this.chatWithUser.bind(this, key, totalUsers[key].name)}>
                                                Chat
                                            </button>
                                        </div>
                                    );
                                }
                            }.bind(this))
                        }
                        <button className="btn btn-danger" onClick={this.logout}>Logout</button>
                    </div>
                );
            }
            else return null;
        }
    }
});

function mapStateToProps(state) {

    return {
        users: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        logout: actions.logoutStart,
        getUsers: actions.getUsers,
        chatWith: actions.chatWithUser,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Main);