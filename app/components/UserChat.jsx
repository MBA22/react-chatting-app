var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');
var {refFirebaseStorage} = require('./../firebase/');

var UserMain = React.createClass({
    componentDidMount: function () {
        this.props.fetchMessages();
    },
    sendMessage: function (e) {
        e.preventDefault();
        if (this.refs.msg.value != "" && this.refs.msg.value != undefined) {
            this.props.sendMsg(this.refs.msg.value);
        }
        this.refs.msg.value = "";
        var element = document.getElementById('chat');
        element.setAttribute("style", "overflow:auto;");
        element.scrollTop = elem.scrollHeight;
    },
    removeChatting: function () {
        this.props.removeChatting();
    },
    uploadImage: function (e) {
        var image = e.target.files[0];
        var imageName = e.target.value.replace(/^.*\\/, '');
        var imageUrl = '';
        var imageStorage = refFirebaseStorage.ref(`images/${imageName}`);
        var uploadTast = imageStorage.put(image);
        uploadTast.on('state_changed', function (snapshot) {
            imageUrl = snapshot.downloadURL;
        }, function (error) {
            console.log(error);
        }, function () {
            this.props.sendMsg(imageUrl);
        }.bind(this));
    },
    render: function () {
        console.log(this.props.user.userKey, this.props.user.userName);
        if (this.props.user.fetching) {
            return (
                <p>Loading.....</p>
            );
        }
        else {
            var messages = this.props.user.messages;

            if (messages != undefined) {
                return (
                    <div className="container">
                        <button className="btn" onClick={this.removeChatting}>Back</button>
                        <h3 className="text-center">Chatting with {this.props.user.userName}</h3>
                        <div className="row">
                            <div className="col-xs-8 col-xs-offset-2" id="chat">
                                {
                                    Object.keys(messages).map(function (key) {

                                        if (String(messages[key].message).indexOf('images') != -1) {
                                            return (
                                                <div>
                                                    <img src={String(messages[key].message)} alt="image" height="200"
                                                         width="200"/><span> by:{messages[key].name}</span>
                                                </div>
                                            );
                                        }
                                        else
                                            return (
                                                <div>
                                                    <li className="list-group-item" key={key}>
                                                        <p>{messages[key].message}<span> by:{messages[key].name}</span></p>
                                                    </li>

                                                </div>
                                            );
                                    }.bind(this))
                                }
                            </div>
                        </div>
                        <form onSubmit={this.sendMessage}>
                            <div className="col-xs-8 form-group">
                                <input className="form-control" ref="msg" type="text" placeholder="message"/>
                            </div>
                            <button className="btn btn-success" onClick={this.sendMessage}>Send</button>
                            <input className="btn btn-info" type="file" onChange={this.uploadImage}/>
                        </form>
                    </div>
                );
            }
            else {
                return (
                    <div className="container">
                        <h3>Chatting with {this.props.user.userName}</h3>
                        <form onSubmit={this.sendMessage}>
                            <input ref="msg" type="text" placeholder="message"/>
                            <button onClick={this.sendMessage}>Send</button>
                            <input type="file" onChange={this.uploadImage}/>
                        </form>
                    </div>
                );
            }
        }
    }
});
function mapStateToProps(state) {

    return {
        user: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        logout: actions.logoutStart,
        sendMsg: actions.sendMessage,
        fetchMessages: actions.getMessages,
        removeChatting: actions.removeChatting,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(UserMain);