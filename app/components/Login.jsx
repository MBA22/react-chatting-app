var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');


var Login = React.createClass({

    doLogin: function (provider) {
        this.props.loginStart(provider);
    },
    render: function () {
        return (
            <div className="container">
                <div className="text-center">
                    <h3>Login</h3>
                    <button className="btn btn-success" onClick={this.doLogin.bind(this, 'github')}>Github</button>
                    <br/>
                    <br/>
                    <button className="btn btn-primary" onClick={this.doLogin.bind(this, 'facebook')}>Facebook</button>
                    <br/>
                    <br/>
                    <button className="btn btn-info" onClick={this.doLogin.bind(this, 'twitter')}>Twitter</button>
                </div>
            </div>
        );
    }
});

function mapStateToProps(state) {

    return {
        data: state.loginlogoutReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        loginStart: actions.loginStart,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Login);