var firebase = require('firebase');
try {
    var config = {
        apiKey: "AIzaSyBMvvPlbyxZuwoSNEFuvfArnhCcvaycfrA",
        authDomain: "react-chat-app-42ab1.firebaseapp.com",
        databaseURL: "https://react-chat-app-42ab1.firebaseio.com",
        storageBucket: "react-chat-app-42ab1.appspot.com",
        messagingSenderId: "91860301336"
    };
    firebase.initializeApp(config);
} catch (error) {
    console.log(error);
}
export var refFirebase = firebase.database();
export var refFirebaseStorage = firebase.storage();
export var myFirebase = firebase;
export var githubProvider = new firebase.auth.GithubAuthProvider();
export var facebookProvider = new firebase.auth.FacebookAuthProvider();
export var twitterProvider = new firebase.auth.TwitterAuthProvider();

