export var loginsignupReducer = function (state = {}, action) {

    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'LOGIN':
            var newState = Object.assign({}, state, {uid: action.uid, displayName: action.name});
            return newState;

        case 'LOGOUT':
            return {};

        case 'DONE_FETCHING_USERS':
            var newState = Object.assign({}, state, {fetching: false, users: action.users});
            return newState;
        case 'START_CHATTING':
            var newState = Object.assign({}, state, {userKey: action.userKey, userName: action.userName});
            return newState;

        case 'DONE_FETCHING_MESSAGES':
            var newState = Object.assign({}, state, {fetching: false, messages: action.messages});
            return newState;
        case'MESSAGE_SENT':
            var newState = Object.assign({}, state, {messageSent: action.messageSent});
            return newState;

        case'REMOVE_CHATTING':
            var newState = Object.assign({}, state, {userKey: null, userName: null});
            return newState;
        default:
            return state;
    }
};
