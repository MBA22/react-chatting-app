var {myFirebase, refFirebase, githubProvider, facebookProvider, twitterProvider}=require('app/firebase/');

var store = require('./../store/configureStore').storeConfig();

export var setUidNameOnLogin = function (uid, name) {
    console.log("In Set uid action", uid, name);
    return {
        type: "LOGIN",
        uid: uid,
        name: name,
    };
};
export var logout = function () {
    console.log("In logout function");

    return {
        type: "LOGOUT",
    };
};
export var loginStart = function (provider) {
    return function (dispatch, getState) {

        if (provider == "github") {
            myFirebase.auth().signInWithPopup(githubProvider).then(function (result) {
                console.log("Authentication worked", result, result.displayName);
                dispatch(setUidNameOnLogin(result.uid, result.displayName));
            }, function (error) {
                console.log("Authentication failed", error);
            });
        }
        else if (provider == "facebook") {
            myFirebase.auth().signInWithPopup(facebookProvider).then(function (result) {
                console.log("Authentication worked", result, result.displayName);
                dispatch(setUidNameOnLogin(result.uid, result.displayName));
            }, function (error) {
                console.log("Authentication failed", error);
            });
        }
        else if (provider == "twitter") {
            myFirebase.auth().signInWithPopup(twitterProvider).then(function (result) {
                console.log("Authentication worked", result, result.displayName);
                dispatch(setUidNameOnLogin(result.uid, result.displayName));
            }, function (error) {
                console.log("Authentication failed", error);
            });
        }
    }
};
export var logoutStart = function () {
    return function (dispatch, getState) {
        return myFirebase.auth().signOut().then(function () {
            console.log("Log out successfull");
            dispatch(logout());
        });
    }
};

export var tryCreateUser = function (userUid, userData) {
    refFirebase.ref('/users').child(userUid).transaction(function (currentUserData) {
        if (currentUserData === null)
            return userData;
    }, function (error, committed) {
        console.log(error, committed);
    });
}
export var startFetching = function () {
    return {
        type: "STARTING_FETCHING",
    };
};
export var doneFetchingUsers = function (user) {
    return {
        type: "DONE_FETCHING_USERS",
        users: user,
    };
};

export var getUsers = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var users = refFirebase.ref('/users');
        users.on('value', function (snapshot) {
            dispatch(doneFetchingUsers(snapshot.val()));
        });
    }
};
export var chatWithUser = function (userKey, userName) {
    return {
        type: "START_CHATTING",
        userKey: userKey,
        userName: userName,
    };
};

export var messageSent = function (message) {
    return {
        type: "MESSAGE_SENT",
        messageSent: message,
    };
};

export var sendMessage = function (message) {
    return function (dispatch, getState) {
        var messageObject = {};
        messageObject.message = message;
        messageObject.name = getState().loginsignupReducer.displayName;
        var currentUid = getState().loginsignupReducer.uid;
        var chattingUserUid = getState().loginsignupReducer.userKey;
        var currentUser = refFirebase.ref(`/users/${currentUid}/chats/${chattingUserUid}`);
        currentUser.push(messageObject);
        var chattingUser = refFirebase.ref(`/users/${chattingUserUid}/chats/${currentUid}`);
        chattingUser.push(messageObject);
        dispatch(messageSent(messageObject));
    };
};

export var doneFetchingMessages = function (messages) {
    return {
        type: "DONE_FETCHING_MESSAGES",
        messages: messages,
    };
};
export var getMessages = function () {
    return function (dispatch, getState) {
        var currentUid = getState().loginsignupReducer.uid;
        var chattingUserUid = getState().loginsignupReducer.userKey;
        var currentUser = refFirebase.ref(`/users/${currentUid}/chats/${chattingUserUid}`);
        currentUser.on('value', function (snapshot) {
            dispatch(doneFetchingMessages(snapshot.val()));
        });
    };
};

export var removeChatting = function () {
    return {
        type: 'REMOVE_CHATTING',
    };
};




